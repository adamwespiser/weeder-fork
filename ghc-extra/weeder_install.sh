#!/bin/sh

# To run, make sure weeder is checked out with a branch that is capable of building (master should be fine)
# After this script is run, if you want to install weeder:
# $ cd weeder; sudo ./Setup install

# stack resolver: lts-15.3
# use stack unpack [package-name] --resolver=lts-15.3
# to download dependencies known to work with eachother
# works with:
# ghc commit: 3a259092a0
# ghc version tag: 8.11.0.20200312

set -e
export STAGE=1
export GHC=${HOME}/projects/ghc/_build/stage${STAGE}/bin/ghc
export GHCPKG=${HOME}/projects/ghc/_build/stage${STAGE}/bin/ghc-pkg
export DIR=${HOME}/projects/ghc/my-lib/
export DIR="."
export HACKAGE_URL="http://hackage.haskell.org/package"

mkdir -p ${DIR}
cd ${DIR}


fetch_pkg () {
  PKGVER=$1
  if [ -d ${PGKVER} ]; then
    echo "_skip fetch $PKGVER"
  else
    echo "_fetching $PKGVER"
    URL_PKG=${HACKAGE_URL}/${PKGVER}/${PKGVER}.tar.gz
    echo ${URL_PKG}
    curl -C - -O ${URL_PKG}
    tar xzf ${PKGVER}.tar.gz
    rm ${PKGVER}.tar.gz
  fi
}

build_pkg () {
  LOCAL_DIR=$1
  results=$($GHCPKG list | grep ${LOCAL_DIR} | wc -l)
  echo $results
  if [ "$results" -eq "1" ]; then
    echo "_skip Build ${LOCAL_DIR}"
  else
    echo "_Building ${LOCAL_DIR}"
    cd ${LOCAL_DIR}
    SETUP=$(ls | grep -E Setup.hs\|.lhs)
    ${GHC} --make ${SETUP}
    ./Setup configure --with-compiler ${GHC} --with-hc-pkg ${GHCPKG}
    ./Setup build
    ./Setup register --inplace --verbose
    rm Setup
    rm Setup.o
    rm Setup.hi
    cd ..
  fi
}


fetch_build_pkg() {
  PKG=$1
  fetch_pkg $PKG
  build_pkg $PKG
}

# git clone git@github.com:ocharles/weeder.git
build_weeder () {
  cd weeder
  ${GHC} --make Setup.hs
  ./Setup configure --with-compiler ${GHC} --with-hc-pkg ${GHCPKG}
  ./Setup build
  ./Setup register --inplace --verbose
  rm Setup.o
  rm Setup.hi
  cd ..
}


# Note: This won't fully work from scratch,
# and some of these cabal dependendencies need to be bumped up

fetch_build_pkg "colour-2.3.5"
fetch_build_pkg "ansi-terminal-0.9.1"
fetch_build_pkg "ansi-wl-pprint-0.6.9"
fetch_build_pkg "transformers-compat-0.6.5"
fetch_build_pkg "optparse-applicative-0.15.1.0"
fetch_build_pkg "base-orphans-0.8.2"
fetch_build_pkg "tagged-0.8.6"
fetch_build_pkg "distributive-0.6.1"
fetch_build_pkg "comonad-5.0.6"
fetch_build_pkg "th-abstraction-0.3.2.0"
fetch_build_pkg "bifunctors-5.5.7"
fetch_build_pkg "call-stack-0.2.0"
fetch_build_pkg "comonad-5.0.6"
fetch_build_pkg "StateVar-1.2"
fetch_build_pkg "contravariant-1.5.2"
fetch_build_pkg "hashable-1.3.0.0"
fetch_build_pkg "unordered-containers-0.2.10.0"
fetch_build_pkg "transformers-base-0.4.5.2"
fetch_build_pkg "semigroupoids-5.3.4"
fetch_build_pkg "profunctors-5.5.2"
fetch_build_pkg "free-5.1.3"
fetch_build_pkg "void-0.7.3"
fetch_build_pkg "semigroups-0.19.1"
fetch_build_pkg "adjunctions-4.4"
fetch_build_pkg "invariant-0.5.3"
fetch_build_pkg "kan-extensions-5.2"
fetch_build_pkg "parallel-3.2.2.0"
fetch_build_pkg "reflection-2.1.5"
fetch_build_pkg "template-haskell-2.15.0.0"
fetch_build_pkg "transformers-compat-0.6.5"
fetch_build_pkg "type-equality-1"
fetch_build_pkg "primitive-0.7.0.1"
fetch_build_pkg "vector-0.12.1.2"
fetch_build_pkg "lens-4.18.1"
fetch_build_pkg "indexed-profunctors-0.1"
fetch_build_pkg "generic-lens-core-2.0.0.0"
fetch_build_pkg "generic-lens-2.0.0.0"
fetch_build_pkg "algebraic-graphs-0.5"

# Note, before you build weeder, make sure it is updated to work with ghc 8.11
build_weeder

