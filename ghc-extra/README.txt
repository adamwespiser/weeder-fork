# Run weeder on ghc
This is a basic guide for how to run weeder on ghc. It includes the steps I took, but it should be noted the following methods will be subject to considerable bit rot, (run March 2020).
The basic idea is to run weeder on .hie files generated during Stage1 of compilation.
The tricky part, is that n order for weeder to work, the ghc version that weeder is compiled with must match the ghc version of the .hie files. Fortunately, you can do this in one compile.

First, build ghc with hadrian and the supplied UserSettings.hs, which will generate .hie files.
You will need to modify UserSettings.hs to inculde a path to store the .hie files (I'm using an absolute path), and if you want, can specifiy which stage, Stage1 or Stage2, to create the .hie files for.

The next step is to build weeder. The version of weeder I used is [a fork by ocharles](https://github.com/ocharles/weeder), and the fork is available here: https://gitlab.haskell.org/adamwespiser/weeder-fork
However, weeder needs quite a few dependencies. I tried to get cabal to install these, but ran into errors.
Instead, I reverted to a manual process of dependency resolution that involved using the latest version of stack (lts-15.2) to find compatible packages, building them via `Setup.hs`, then registering them with the Stage1 `ghc-pkg`.
The supplied script, "weeder_install" does most of the work, but some of libraries will need their cabal files updated to included higher version bounds. "weeder_intall" is built with the stage2 compiler.

After you finishing installing weeder, you can run it with the following:
$ cd ghc/hiedir/stage1
$ weeder . --root "main Main"


# Run on Stage2 .hiefiles
There is also a run of weeder on the .hie files generated in stage2.
This was done by Modifying UserSettings to in two places.
he first is bumping the finalStageStage to 3, and the next is change the predicate and path in `userArgs` to reflect stage2 over stage1.



